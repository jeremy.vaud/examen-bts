<?php
/*
 * Recherche de compte client
 */
include 'lib/init.php';
try {
    if(!isConnect()){
        throw new Exception('error');
    }
    $utilisateur = new utilisateur($_SESSION['id']);
    if($utilisateur->get('statu') !== 'vendeur'){
        throw new Exception('error');
    }
    if(!isset($_POST['search'])){
        throw new Exception('error');
    }
    $tabObjet = $utilisateur->search($_POST['search']);
    $template = new template;
    $template->listeObjet($tabObjet, 'list-client');
    $template->display();  
} catch (Exception $exc) {
    echo $exc->getMessage();
}
