<?php
/*
 * Création du formulaire ou Insertion pour créer une novelle vente depuis un compte vendeur 
 */
include 'lib/init.php';
try {
    if(!isConnect()){
        throw new Exception('error');
    }
    $utilisateur = new utilisateur($_SESSION['id']);
    if($utilisateur->get('statu') !== 'vendeur'){
        throw new Exception('error');
    }
    if(!isset($_POST['action'])){
        throw new Exception('error');
    }
    if($_POST['action'] === 'form'){
        // Création formulaire
        $produit = new produits;
        $tabProduit = $produit->liste();
        if($tabProduit === []){
            throw new Exception('error');
        }
        $form = new template();
        $form->select("Choix du produit", 'produit',$tabProduit, 'libelle');
        $form->hiddenInput('utilisateur', htmlentities($_POST['id']));
        $form->form(['num-serie'=>'texte','date'=>'date']);
        $form->display();
    }elseif ($_POST['action'] === 'submit') {
        // Insertion vente
        $vente = new vente;
        $vente->setFromTab($_POST);
        $dateTime = $_POST['date']." ".$_POST['time'];
        $vente->set('date', $dateTime);
        if(!$vente->insert()){
            throw new Exception('error');
        }
    }
    
} catch (Exception $exc) {
    echo $exc->getMessage();
}
