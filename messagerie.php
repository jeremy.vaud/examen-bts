<?php
include 'lib/init.php';
// Verif connecté
if(!isConnect()){
    header('Location: index.php');
    exit;
}
// Verif utilisateur
$utilisateur = new utilisateur($_SESSION['id']);
if(!($utilisateur->get('statu') === 'client' || $utilisateur->get('statu') === 'technicien')){
    header('Location: index.php');
    exit;
}
// Verif url
if(!isset($_GET['ticket'])){
    header('Location: index.php');
    exit;
}
// Préparation template
$header = new template;
$header->setTemplate('header/connect');
$listMessage = new template;
$form = new template;
try {
    $ticket = new ticket;
    $produit = new produits;
    if(!$ticket->loadFromId($_GET['ticket'])){        
        throw new Exception("Cette demande d'assistance est introuvable");
    }
    if($ticket->get('statu') === 'fermer'){
        throw new Exception("Cette demande d'assistance à été cloturé");
    }
    $produit->loadFromId($ticket->getLink('vente')->get('produits'));
    $msg = new message;
    $tabMsg = $msg->loadMessagerie($ticket->get('id'));
    $listMessage->listeObjet($tabMsg, 'list-message');
    $form->form(["reponse"=>"textarea"],"",["utilisateur"=>$utilisateur->get('id'),"ticket"=>$ticket->get('id')]);
    include 'template/page/messagerie.php';
} catch (Exception $exc) {
    $message = $exc->getMessage();
    include 'template/page/empty.php';
}

