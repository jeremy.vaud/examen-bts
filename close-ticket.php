<?php
/*
 * Fermeture de ticket
 */
include 'lib/init.php';
try {
    if(!isConnect()){
        throw new Exception('error');
    }
    $utilisateur = new utilisateur($_SESSION['id']);
    if(!($utilisateur->get('statu') === 'client' || $utilisateur->get('statu') === 'technicien')){
        throw new Exception('error');
    }
    if(!isset($_POST['ticket'])){
        throw new Exception('error');
    }
    $ticket = new ticket;
    if(!$ticket->loadFromId($_POST['ticket'])){
        throw new Exception('error');
    }
    $ticket->set('statu','fermer');
    if(!$ticket->update()){
        throw new Exception('error');
    }
    echo 'fermer';   
} catch (Exception $exc) {
    echo $exc->getMessage();
}