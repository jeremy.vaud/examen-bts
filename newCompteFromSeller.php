<?php
/*
 * Création du formulaire ou Insertion pour créer un noveau client depuis un compte vendeur 
 */
include 'lib/init.php';
try {
    if(!isConnect()){
        throw new Exception('error');
    }
    $utilisateur = new utilisateur($_SESSION['id']);
    if($utilisateur->get('statu') !== 'vendeur'){
        throw new Exception('error');
    }
    if(!isset($_POST['action'])){
        throw new Exception('error');
    }
    if($_POST['action'] === 'form'){
        // Création formulaire
        $form = new template;
        $form->form(['nom'=>'texte','prenom'=>'texte','email'=>'texte','password-1'=>'password','password-2'=>'password']);
        $form->display();
    }elseif ($_POST['action'] === 'submit') {
        // Insertion client
        $client = new utilisateur;
        $client->setFromTab($_POST);
        if(!$client->availableEmail()){
            throw new Exception('email');
        }
        $password = password_hash($_POST['password'], PASSWORD_DEFAULT);
        $client->set('password', $password);
        $client->set('statu', 'client');
        if(!$client->insert()){
            throw new Exception('error');
        }
        $tabObjet = [$client];
        $template = new template;
        $template->listeObjet($tabObjet, 'list-client');
        $template->display();
    }
    
} catch (Exception $exc) {
    echo $exc->getMessage();
}
