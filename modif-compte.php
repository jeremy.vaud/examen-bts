<?php
/*
 * Modification email ou mot de passe d'un compte
 */
include 'lib/init.php';
try {
    if(!isConnect()){
        throw new Exception('error');
    }
    $utilisateur = new utilisateur($_SESSION['id']);
    if($_POST['mode'] === 'email' && $utilisateur->get('statu') === 'client'){
        // Modifiction email
        $utilisateur->set('email', $_POST['email']);
    }elseif ($_POST['mode'] === 'mot de passe' && ($utilisateur->get('statu') === 'client' || $utilisateur->get('statu') === 'vendeur' || $utilisateur->get('statu') === 'technicien')) {
        // Modifiction mot de passe
        if(!password_verify($_POST['oldPassword'], $utilisateur->get('password'))){
            throw new Exception('error-pass');
        }
        $password = password_hash($_POST['newPassword'], PASSWORD_DEFAULT);
        $utilisateur->set('password', $password);
    }else{
        throw new Exception('error');
    }
    // Update
    if(!$utilisateur->update()){
        throw new Exception('error');
    }
    if($_POST['mode'] === 'email'){
        echo $utilisateur->get('email');
    }else{
        echo 'password';
    }
} catch (Exception $exc) {
    echo $exc->getMessage();
}

