<?php
/*
 * compte utilisateur
 */

// Si l'on est pas connecté ou si l'on ne peut pas charger l'utilisateur on redirige vers l'index
include 'lib/init.php';
if(!isConnect()){
    header('Location: index.php');
    exit;
}
$utilisateur = new utilisateur;
if(!$utilisateur->loadFromId($_SESSION['id'])){
    header('Location: index.php');
    exit;
}
$header = new template;
$header->setTemplate('header/connect');
include 'template/page/compte.php';