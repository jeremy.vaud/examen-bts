<?php
/* 
 * Connexion
 */
include 'lib/init.php';
try {
    if(!isset($_POST['email']) || !isset($_POST['password'])){
        throw new Exception("Une erreur est survenu, veuillez réessayer");
    }
    $utilisateur = new utilisateur;
    $utilisateur->set('email', $_POST['email']);
    $verif = $utilisateur->connect($_POST['password']);
    if($verif === 'error-execute'){
        throw new Exception("Une erreur est survenu, veuillez réessayer");
    }
    if($verif === "error-email"){
        throw new Exception("Votre email est incorrect, veuillez réessayer");
    }
    if($verif === "error-password"){
        throw new Exception("Votre mot de passe est incorrect, veuillez réessayer");
    }
    connexion($utilisateur->get('id'));
    header('Location:index.php?msg');  
} catch (Exception $exc) {
    $message = $exc->getMessage();
    $header = new template;
    $header->setTemplate('header/notConnect');  
    $form = new template;
    $form->form(["email"=>"texte","password"=>"password"], 'connexion.php');
    $titre = 'Connexion à votre compte';
    include 'template/page/form.php';
    exit();
}