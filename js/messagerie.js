/* 
 * Gestion de la messagerie
 */
$(document).ready(function(){
    $('form').children('input[type=submit]').on('click',newMsg);
    $('#close').on('click',close);
});

function newMsg(){
    // Role: Ajax pour enregistrement d'un nouveau message
    // Param: Néant
    // Retour: Néant
    var msg = $('textarea').val();
    var user = $('input[name=utilisateur]').val();
    var ticket = $('input[name=ticket]').val();
    if(msg === ""){
        return false;
    }
    $.ajax('new-msg.php',{
        method: 'POST',
        data: {message:msg,utilisateur:user,ticket:ticket},
        success: majMsg,
        error: error
    });
    return false;
}

function majMsg(data){
    // Role: Maj messagerie
    // Param: data -> contenu html
    // Retour: Néant
    if(data === 'error'){
        error();
    }
    $('textarea').val('');
    $('.listMessage').html(data);
}

function error(){
    // Role: Affiche message d'erreur quand une requete ajax à échoué
    // Param: Néant
    // Retour: Néant
    $('.msg').children('p').html('Une erreur est survenu');
}

function close(){
    // Role: Affiche message de validation de fin dassistance
    // Param: Néant
    // Retour: Néant
    $('#p-close').addClass('hidden');
    $('#p-valid').removeClass('hidden');
    $('#confirm').on('click',confirm);
    $('#cancel').on('click',cancel);    
}

function confirm(){
    // Role: Confirmer la cloture
    // Param: Néant
    // Retour: Néant
    var ticket = $('input[name=ticket]').val();
    $.ajax('close-ticket.php',{
        method: 'POST',
        data: {ticket:ticket},
        success: redirect,
        error: error
    });
}

function cancel(){
    // Role: Annuler la cloture
    // Param: Néant
    // Retour: Néant
    $('#p-close').removeClass('hidden');
    $('#p-valid').addClass('hidden');
}

function redirect(data){
    // Role: Afficher la confirmation de la fermeture du ticket
    // Param: data->"error" si une erreur c'est produite lors de la fermeture du ticket
    // Retour: Néant
    if(data === 'error'){
        $('.msg').children('p').html('Une erreur est survenu lors de la cloture');
    }else if(data === 'fermer'){
        $('main').html('');
        $('.msg').children('p').html("La demande d'assistance à bien été cloturé");
    }
}