/*
 * Gestion bouton pages mon compte
 */
$(document).ready(function(){
    // Affichage boutton modif email si l'utilisateur est client et ajout event
    if($('#statu').val() === 'client'){
        $('#btnEmail').removeClass('hidden');
        $('#btnEmail').on('click',form);    
    }
    $('#btnPassword').on('click',form);
    $('#submit').on('click',submit);
    $('#submit2').on('click',submit);
    $('#input-1').on('change',verif);
    $('#input-2').on('change',verif);
    $('#input-3').on('change',verif);
});

var modeModif = 'none'

function form(){
    // Role: Afficher formulaure de modifification
    // Param: Néant
    // Retour: Néant
    var btn = $(this).attr('id');
    $('.msg').children('p').html('');
    if(btn === 'btnEmail' && modeModif !== 'email'){
        $("#modif").children('h2').html('Modification de votre adresse email');
        $('#p-1').html('Nouvelle adresse email: ');
        $('#p-2').html('');
        $('#p-3').html(''); 
        $('#p-1').next('span').html("");
        $('#p-2').next('span').html("");
        $('#p-3').next('span').html("");
        $('#input-1').val('');
        $('#input-1').attr("type",'text');
        $('#input-1').removeClass('border-red');
        $('#input-2').val('');
        $('#input-2').attr("type",'hidden');
        $('#input-2').removeClass('border-red');
        $('#input-3').val('');
        $('#input-3').attr("type",'hidden');
        $('#input-3').removeClass('border-red');
        $('#submit').attr("type",'submit');
        $('#submit2').attr("type",'hidden');
        modeModif = 'email';
    }else if(btn === 'btnPassword' && modeModif !== 'mot de passe'){
        $("#modif").children('h2').html('Modification de votre mot de passe');
        $('#p-1').html('Ancien mot de passe: ');
        $('#p-2').html('Nouveau mot de passe: ');
        $('#p-3').html('Nouveau mot de passe: ');
        $('#p-1').next('span').html("");
        $('#p-2').next('span').html("");
        $('#p-3').next('span').html("");
        $('#input-1').val('');
        $('#input-1').attr("type",'password');
        $('#input-1').removeClass('border-red');
        $('#input-2').val('');
        $('#input-2').attr("type",'password');
        $('#input-2').removeClass('border-red');
        $('#input-3').val('');
        $('#input-3').attr("type",'password');
        $('#input-3').removeClass('border-red');
        $('#submit2').attr("type",'submit');
        $('#submit').attr("type",'hidden');
        modeModif = 'mot de passe';
    }  
}

function submit(){
    // Role: Soumettre la modification si les champ sont bien remplit
    // Param: Néant
    // Retour: Néant
    if(modeModif === 'email'){
        var email = $('#input-1').val();
        if(!(/^[a-zA-Z1-9]([a-zA-Z1-9_-]+[.]{0,1})+[a-zA-Z1-9_-]{1,}[@]{1}[a-zA-Z1-9]([a-zA-Z1-9_-]+[.]{0,1})+[a-zA-Z1-9_-]{2,}$/.test(email))){
            $('#p-1').next('span').html("Email invalide");
            $('#input-1').addClass('border-red');
            return;
        }
        $.ajax('modif-compte.php',{
            method: 'POST',
            data: {mode:modeModif,email:email},
            success: success,
            error:error
        });
    }else if(modeModif === 'mot de passe'){
        var oldPass = $('#input-1').val();
        var newPass1 = $('#input-2').val();
        var newPass2 = $('#input-3').val();
        var err = 0;
        if(!(/^[a-zA-Z1-9_.:?ù*+-]{4,8}$/.test(oldPass))){
            $('#input-1').addClass('border-red');
            $('#input-1').prev('div').children('span').html('Mot de passe invalide');
            err++;
        }
        if(!(/^[a-zA-Z1-9_.:?ù*+-]{4,8}$/.test(newPass1))){
            $('#input-2').addClass('border-red');
            $('#input-2').prev('div').children('span').html('Mot de passe invalide');
            err++;
        }
        if(!(/^[a-zA-Z1-9_.:?ù*+-]{4,8}$/.test(newPass2))){
            $('#input-3').addClass('border-red');
            $('#input-3').prev('div').children('span').html('Mot de passe invalide');
            err++;
        }
        if(newPass1 !== newPass2){
            $('#input-2').prev('div').children('span').html('Vos nouveaux mots de passe doivent être identique');
            err++;
        }
        if(err !== 0){
            return;
        }
        $.ajax('modif-compte.php',{
            method: 'POST',
            data: {mode:modeModif,newPassword:newPass1,oldPassword:oldPass},
            success: success,
            error:error
        });
    }
}

function success(data){
    // Role: Afficher la confirmation de la modification
    // Param: data -> chaine de caractere recus après requete ajax
    // Retour: Néant
    if(data === 'error'){
        error();
    }else if(data === 'error-pass'){
        $('.msg').children('p').html("Votre ancien mot de passe n'est pas valide");
    }else{
        $('.msg').children('p').html('La modifiction de votre '+modeModif+' à été validé');
        $('#submit').attr("type",'hidden');
        $('#submit2').attr("type",'hidden');
        $("#modif").children('h2').html('');
        $('#p-1').html('');
        $('#p-2').html('');
        $('#p-3').html('');       
        $('#input-1').val('');
        $('#input-1').attr("type",'hidden');
        $('#input-2').val('');
        $('#input-2').attr("type",'hidden');
        $('#input-3').val('');
        $('#input-3').attr("type",'hidden');
        $('#input-2').prev('div').children('span').html('');
        modeModif = 'none';
        if(data !== 'password'){
            $('#userEmail').html(data);
        }
    }
}

function verif(){
    // Role: Vérifier la validité du champ et affiché un message si le champ n'est pas valid
    // Param: Néant
    // Retour: Néant
    var texte = $(this).val();
    if(modeModif === 'email'){
        if(!(/^[a-zA-Z1-9]([a-zA-Z1-9_-]+[.]{0,1})+[a-zA-Z1-9_-]{1,}[@]{1}[a-zA-Z1-9]([a-zA-Z1-9_-]+[.]{0,1})+[a-zA-Z1-9_-]{2,}$/.test(texte))){
            $('#p-1').next('span').html("Email invalide");
            $(this).addClass('border-red');
        }else{
            $('#p-1').next('span').html("");
            $(this).removeClass('border-red');
        }
    }
    if(modeModif === 'mot de passe'){
        if(!(/^[a-zA-Z1-9_.:?ù*+-]{4,8}$/.test(texte))){
            $(this).addClass('border-red');
            $(this).prev('div').children('span').html('Mot de passe invalide');
        }else{
            $(this).removeClass('border-red');
            $(this).prev('div').children('span').html('');
        }
    }
}

function error(){
    $('.msg').children('p').html('Une erreur est survenu lors de la modifiction votre '+modeModif);
}