/* 
 * Page Acceuil vendeur
 */
$(document).ready(function(){
    // Ajout evenement sur les boutons
    $('#search').on('click',search);
    $('#newCompte').on('click',newCompte);
});

function search(){
    // Role : Rechercher un compte client
    // Param: Néant
    // Retour: Néant
    var search = $("#search-barr").val();
    $.ajax('search-client.php',{
        method: 'POST',
        data: {search:search},
        success: afficheSearch,
        error: error
    });
}    

function afficheSearch(data){
    // Role: Afficher le résultat de la recherche
    // Param: Néant
    // Retour: Néant
    if(data === 'error'){
        error();
        return;
    }
    $('.msg').children('p').html('');
    if(data === ""){
        $('#titre').html("Aucun résultat ne correspond à votre recherche");
        $('.result').html(data);
    }else{
        $('#titre').html("Résultat de recherche");
        $('.result').html(data);
        $('.addSell').on('click',addSell);
    }
}


function newCompte(){
    // Role: Requete ajax pour récupérer le formulaire de création de compte
    // Param: Néant
    // Retour: Néant
    $.ajax('newCompteFromSeller.php',{
        method: 'POST',
        data: {action:'form'},
        success: afficheForm,
        error: error            
    });
}

function afficheForm(data){
    // Role: Afficher le formulaire de création de compte
    // Param: Néant
    // Retour: Néant
    if(data === 'error'){
        error();
        return;
    }
    $('#titre').html("Création d'un compte client");
    $('.result').html(data);
    $('form').on('submit',submitClient);
    $('#nom').on('change',verif);
    $('#prenom').on('change',verif);
    $('#email').on('change',verif);
    $('#password-1').on('change',verif);
    $('#password-2').on('change',verif);
}

function submitClient(){
    // Role: Soumettre le formulaire de création de client
    // Param: Néant
    // Retour: false
    var nom = $('#nom').val();
    var prenom = $('#prenom').val();
    var email = $('#email').val();
    var password1 = $('#password-1').val();
    var password2 = $('#password-2').val();
    var error = false;
    if(nom === ""){
        $('#nom').prev('label').children('span').html("Le champ nom ne peut pas être vide");
        $('#nom').addClass('border-red');
        error = true;
    }
    if(prenom === ""){
        $('#prenom').prev('label').children('span').html("Le champ prenom ne peut pas être vide");
        $('#prenom').addClass('border-red');
        error = true;
    }
    if(!(/^[a-zA-Z1-9]([a-zA-Z1-9_-]+[.]{0,1})+[a-zA-Z1-9_-]{1,}[@]{1}[a-zA-Z1-9]([a-zA-Z1-9_-]+[.]{0,1})+[a-zA-Z1-9_-]{2,}$/.test(email))){
        $('#email').prev('label').children('span').html("Le champ email n'est pas valide");
        $('#email').addClass('border-red');
        error = true;
    }
    if(!(/^[a-zA-Z1-9_.:?ù*+-]{4,8}$/.test(password1))){
        $('#password-1').prev('label').children('span').html("Le mot de passe doit contenir 4 à 8 caractères");
        $('#password-1').addClass('border-red');
        error = true;
    }
    if(!(/^[a-zA-Z1-9_.:?ù*+-]{4,8}$/.test(password2))){
        $('#password-2').prev('label').children('span').html("Le mot de passe doit contenir 4 à 8 caractères");
        $('#password-2').addClass('border-red');
        error = true;
    }
    if(password1 !== password2){
        $('#password-1').prev('label').children('span').html("Les mot de passe ne sont pas identiques");
        error = true;
    }
    if(error){
        return false;
    }
    $.ajax('newCompteFromSeller.php',{
        method: 'POST',
        data: {action:'submit',nom:nom,prenom:prenom,email:email,password:password1},
        success: successCompte,
        error: error            
    });
    return false;
}

function successCompte(data){
    // Role: Informer que le compte utilisateur à été crée
    // Param: data -> chaine de caractere recus de la requete ajax
    // Retour: Néant
    if(data === 'error'){
        error();
        return;
    }
    if(data === 'email'){
        $('.msg').children('p').html('Cette adresse email existe déja');
        return;
    }
    $('.msg').children('p').html('Le compte à été crée avec succès');
    $('#titre').html("Client");
    $('.result').html(data);
    $('.addSell').on('click',addSell);
}

function addSell(){
    // Role: Récupérer le formulaire d'ajout de vente pour un client
    // Param: Néant
    // Retour: Néant
    var id = $(this).attr('data-id');
    $.ajax('new-sell.php',{
        method: 'POST',
        data: {action:'form',id:id},
        success: formSell,
        error: error            
    });
}

function formSell(data){
    // Role: Afficher le formulaire d'ajout de vente pour un client
    // Param: data -> chaine de caractere recus de la requete ajax
    // Retour: false
    if(data === 'error'){
        error();
    }else{
        $('#titre').html("Ajout d'une vente");
        $('.result').html(data);
        $('form').on('submit',submitSell);
        $('#num-serie').on('change',verif);
    }
}

function submitSell(){
    // Role: Soumettre le formulaire de d'ajout de vente
    // Param: Néant
    // Retour: false
    var utilisateur = $('#utilisateur').val();
    var produits = $('#produit').val();
    var numserie = $('#num-serie').val();
    var date = $('#date-date').val();
    var time = $('#date-time').val();
    var error = false;
    if(!(/^[0-9A-Z]{15}$/.test(numserie))){
        $('#num-serie').prev('label').children('span').html("Le numéro de serie n'est pas valide");
        $('#num-serie').addClass('border-red');
        error = true;
    }
    if(date === "" || time === ""){
        $('#date-date').prev('label').children('span').html("La date et l'heure n'ont pas été renseigné");
        error = true;
    }
    if(error){
        return false;
    }
    $.ajax('new-sell.php',{
        method: 'POST',
        data: {action:'submit',utilisateur:utilisateur,produits:produits,numserie:numserie,date:date,time:time},
        success: successSell,
        error: error            
    });
    return false;
}

function successSell(data){
    // Role: Informer que le compte la vente à bien été enregistré
    // Param: data -> chaine de caractere recus de la requete ajax
    // Retour: Néant
    if(data === 'error'){
        error();
    }else{
        $('.msg').children('p').html('La vente à bien été enregistré');
        $('#titre').html("");
        $('.result').html('');
    }
}

function verif(){
    // Role: Vérifier la validité du champ et affiché un message si le champ n'est pas valid
    // Param: Néant
    // Retour: Néant
    var texte = $(this).val();
    if($(this).attr('id') === 'num-serie'){
        if(!(/^[0-9A-Z]{15}$/.test(texte))){
            $('#num-serie').prev('label').children('span').html("Le numéro de serie n'est pas valide");
            $('#num-serie').addClass('border-red');
        }else{
            $('#num-serie').prev('label').children('span').html("");
            $('#num-serie').removeClass('border-red');
        }       
    }else if($(this).attr('id') === 'nom'){
        if(texte === ''){
            $('#nom').prev('label').children('span').html("Le champ nom ne peut pas être vide");
            $('#nom').addClass('border-red');
        }else{
            $('#nom').prev('label').children('span').html("");
            $('#nom').removeClass('border-red');
        }
    }else if($(this).attr('id') === 'prenom'){
        if(texte === ''){
            $('#prenom').prev('label').children('span').html("Le champ prenom ne peut pas être vide");
            $('#prenom').addClass('border-red');
        }else{
            $('#prenom').prev('label').children('span').html("");
            $('#prenom').removeClass('border-red');
        }
    }else if($(this).attr('id') === 'email'){
        if(!(/^[a-zA-Z1-9]([a-zA-Z1-9_-]+[.]{0,1})+[a-zA-Z1-9_-]{1,}[@]{1}[a-zA-Z1-9]([a-zA-Z1-9_-]+[.]{0,1})+[a-zA-Z1-9_-]{2,}$/.test(texte))){
            $('#email').prev('label').children('span').html("Le champ email n'est pas valide");
            $('#email').addClass('border-red');
        }else{
            $('#email').prev('label').children('span').html("");
            $('#email').removeClass('border-red');
        }
    }else if($(this).attr('id') === 'password-1'){
        if(!(/^[a-zA-Z1-9_.:?ù*+-]{4,8}$/.test(texte))){
            $('#password-1').prev('label').children('span').html("Le mot de passe doit contenir 4 à 8 caractères");
            $('#password-1').addClass('border-red');
        }else{
            $('#password-1').prev('label').children('span').html("");
            $('#password-1').removeClass('border-red');
        }
    }else if($(this).attr('id') === 'password-2'){
        if(!(/^[a-zA-Z1-9_.:?ù*+-]{4,8}$/.test(texte))){
            $('#password-2').prev('label').children('span').html("Le mot de passe doit contenir 4 à 8 caractères");
            $('#password-2').addClass('border-red');
        }else{
            $('#password-2').prev('label').children('span').html("");
            $('#password-2').removeClass('border-red');
        }
    }
}

function error(){
    // Role: Affiche message d'erreur quand une requete ajax à échoué
    // Param: Néant
    // Retour: Néant
    $('.msg').children('p').html('Une erreur est survenu');
}