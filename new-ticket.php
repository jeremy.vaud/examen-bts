<?php
/* 
 * Création nouveau ticket
 */

include 'lib/init.php';
if(!isConnect()){
    header('Location: index.php');
    exit;
}
$utilisateur = new utilisateur($_SESSION['id']);
if($utilisateur->get('statu' !== 'client')){
    header('Location: index.php');
    exit;
}
$ticket = new ticket;
$ticket->setFromTab($_POST);
$ticket->set('statu','ouvert');
$ticket->set('date', date('Y-m-d H:i:s'));
$ticket->set('newmsg','client');
$ticket->set('utilisateur',$_SESSION['id']);
$ticket->insert();
header('Location: index.php');
