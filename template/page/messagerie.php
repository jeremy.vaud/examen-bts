<!DOCTYPE html>
<!-- Page pour messagerie-->
<html>
    <?php include 'template/fragment/head.php'; ?>
    <body>
        <?php $header->display(); ?>
        <?php include 'template/fragment/msg.php'; ?>
        <main  class="messagerie">           
            <div>
                <h2>Produit</h2>
                <p>Produit: <?=$produit->get('libelle',true)?> <span>ref: <?=$produit->get('ref',true)?></span></p>
                <p>Catégorie: <?=$produit->getLink('categories')->get('libelle',true)?></p>
                <p>Numéro de série: <?=$ticket->getLink('vente')->get('numserie',true)?></p>  
            </div>
            <div>
                <h2>Demande d'assistance</h2>
                <h3><?=$ticket->get('titre',true)?></h3>
                <p><?=$ticket->get('demande',true)?></p>
                <p id='p-close'>Si le probleme est résolu, vous pouvez clore cette demande: <button id='close'>Mettre fin à l'assistance</button></p>
                <p id='p-valid' class='hidden error'>Attention cette action cloturera definitivement cette page: <button id='confirm'>Valider</button><button id='cancel'>Annuler</button></p>
            </div>
            <h2>Messagerie</h2>
            <div class="listMessage">
                <?php $listMessage->display(); ?>
            </div>    
            <?php $form->display(); ?>
        </main>
        <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
        <script src="js/messagerie.js" type="text/javascript"></script>
    </body>
</html>
