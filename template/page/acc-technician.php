<!DOCTYPE html>
<!-- Page pour accueil technicien-->
<html>
    <?php include 'template/fragment/head.php'; ?>
    <body>
        <?php $header->display(); ?>
        <?php include 'template/fragment/msg.php'; ?>
        <main class="tech"> 
            <div>
                <h2>Nouveaux Tickets</h2>
                <?php $newTicket->display()?>
            </div>
            <div>
                <h2>Nouveaux Messages</h2>
                <?php $newMsg->display()?>
            </div>
            <div>
                <h2>En attente</h2>
                <?php $attente->display()?>
            </div>
        </main>
    </body>
</html>
