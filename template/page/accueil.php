<!DOCTYPE html>
<!-- Page pour accueil-->
<html>
    <?php include 'template/fragment/head.php'; ?>
    <body>
        <?php $header->display(); ?>
        <?php include 'template/fragment/msg.php'; ?>
        <main> 
            <?php $main->display(); ?>
        </main>
        <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
        <script src="<?=$script?>"></script>
    </body>
</html>
