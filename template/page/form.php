<!DOCTYPE html>
<!-- Page pour formulaire-->
<html>
    <?php include 'template/fragment/head.php'; ?>
    <body>
        <?php $header->display(); ?>
        <?php include 'template/fragment/msg.php'; ?>
        <main>
            <h2><?=$titre?></h2>
            <?php $form->display(); ?>
        </main>
    </body>
</html>
