<!DOCTYPE html>
<!-- Page pour formulaire-->
<html>
    <?php include 'template/fragment/head.php'; ?>
    <body>
        <?php $header->display(); ?>
        <?php include 'template/fragment/msg.php'; ?>
        <main>
            <div>
                <h1>Mon compte</h1>
                <p>Nom: <?=$utilisateur->get('nom',true)?></p>
                <p>Prenom: <?=$utilisateur->get('prenom',true)?></p>
                <p>Email: <span id="userEmail"><?=$utilisateur->get('email',true)?></span> <button id='btnEmail' class='hidden'>Modifier</button></p>
                <p>Statu: <?= ucfirst($utilisateur->get('statu',true))?></p>
                <button id ='btnPassword'>Modifier mon mot de passe</button>
            </div>
            <div id="modif">
                <h2></h2>
                <input type="hidden" id='statu' value="<?=$utilisateur->get('statu',true)?>"/>            <div>      
                    <label id='p-1'></label>
                    <span class="error"></span>
                </div>                
                <input type="hidden" id='input-1'/>
                <input type="hidden" id='submit' value="Modifier"/>
                <div>      
                    <label id='p-2'></label>
                    <span class="error"></span>
                </div>  
                <input type="hidden" id='input-2'/>
                <div>      
                    <label id='p-3'></label>
                    <span class="error"></span>
                </div>  
                <input type="hidden" id='input-3'/>
                <input type="hidden" id='submit2' value="Modifier"/>
            </div>
        </main>
        <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
        <script src="js/compte.js" type="text/javascript"></script>
    </body>
</html>
