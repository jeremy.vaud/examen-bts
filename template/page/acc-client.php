<!DOCTYPE html>
<!-- Page pour accueil client-->
<html>
    <?php include 'template/fragment/head.php'; ?>
    <body>
        <?php $header->display(); ?>
        <?php include 'template/fragment/msg.php'; ?>
        <main class="client"> 
            <div>  
                <h2>Mes achats:</h2>
                <?php $achat->display()?>
            </div>
            <div> 
                <h2>Mes demandes d'assistance:</h2>
                <div>
                    <h3>Nouvelles réponses:</h3>
                    <?php $newAnswer->display()?>
                </div>
                <div>
                    <h3>En attente de réponses:</h3>
                    <?php $waitAnswer->display()?>
                </div>
                
            </div>       
        </main>
    </body>
</html>
