<?php

/* 
 * Fonctions de gestion de session
 */

function isConnect(){
    // Role: Vérifier que l'utilisateur est connecté
    // Param: Néant
    // Retour: true si connecté,false sinon
    if(isset($_SESSION['id'])){
        if($_SESSION['id'] > 0){
            return true;
        }
    }
    return false;
}

function connexion($id){
    // Role: Se conneter
    // Param: $id -> id de l'utilisateur
    // Retour: Néant
    $_SESSION['id'] = $id;
}

function deconnexion(){
    // Role: Se deconneter
    // Param: Néant
    // Retour: Néant
    $_SESSION['id'] = 0;
}