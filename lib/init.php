<?php

/* 
 * Initialisation chargement des classes et fonctions
 */

// Affichage erreur
ini_set(display_errors,1);
error_reporting(E_ALL);

// Chargement des classes
function autoload($class){
    include 'class/'.$class.'.php';
}
spl_autoload_register('autoload');

// Chargement des fonctions
include 'lib/session.php';

// Démarage session
session_start();

// Initialisation message
$message = "";