<?php
/* 
 * Formulaire création ticket ou messagerie ticket
 */
include 'lib/init.php';
if(!isConnect()){
    // Non connecté on affiche l'écran de connexion
    $message = "Vous devez vous connecter pour accéder à cette page";
    $header = new template;
    $header->setTemplate('header/notConnect');  
    $form = new template;
    $form->form(["email"=>"texte","password"=>"password"], 'connexion.php');
    $titre = 'Connexion à votre compte';
    include 'template/page/form.php';
}else{
    // On est connecté
    if(isset($_GET['vente'])){
        $vente = new vente;
        if(!$vente->loadFromId($_GET['vente'])){
            // La vente n'as pas été trouvé on redirige vers l'acceuil
        header('Location: index.php');
        exit;
        }
    }else{
        // L'url n'est pas bonne on redirige vers l'acceuil
        header('Location: index.php');
        exit;
    }
    // La vente existe
    $utilisateur = new utilisateur($_SESSION['id']);
    if($utilisateur->get('statu') !== 'client'){
       // L'utilisateur n'est pas un client, on le redirige donc vers l'acceuil
       header('Location: index.php');
       exit;
    }
    // formulaire création ticket
    $header = new template;
    $header->setTemplate('header/connect'); 
    $titre = "Demande d'assistance pour ".$vente->getLink('produits')->get('libelle',true);
    $form = new template;
    $form->form(['titre'=>'texte','demande'=>'textarea'],'new-ticket.php',['vente'=>$vente->get('id')]);
    include 'template/page/form.php';    
}
