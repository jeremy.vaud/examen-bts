<?php

/*
 * Classe produits
 */

class produits extends table{
    
    // Constructeur
    public function __construct($id = 0){
        // Role: Construction d'un objet produits
        // Param: Néant
        // Retour: Néant
        $this->champ = ["ref"=>"","libelle"=>"","description"=>"","categories"=>"","pv"=>"","dispo"=>""];
        $this->link = ["categories"=>new categories];
        parent::__construct($id);
    }
    
}
