<?php

/*
 * Classe template
 */

class template{
    
    // Attributs
    protected $html = "";
    
    // Méthodes
    public function display(){
        // Role: Afficher le template
        // Param: Néant
        // Retour: Néant
        echo $this->html;
    }
    
    public function setTemplate($template){
        // Role: Affecter un template à l'attribut html
        // Param: $template->nom du template
        // Retour: Néant
        ob_start();
        include 'template/fragment/'.$template.'.php';
        $this->html .= ob_get_contents();
        ob_end_clean();
    }
    
    public function get(){
        // Role: Recuperer la valeur de l'attribut html
        // Param: Néant
        // Retour: l'attribut html
        return $this->html;
    }
    
    public function form($table,$action = '',$tableHidden = []){
        // Role: Créer un formulaire
        // Retour: Néant
        // Param: $table->tableau des input du formulaire [nom => type]
        //        $action->action du form
        //         $tableHidden->pour les input caché [nom => value]
        ob_start();
        if($action === ''){
            echo "<form>";
        }else{
            echo "<form method = 'POST' action = '$action'>";
        }        
        foreach ($table as $nom => $type){
            echo "<label for='$nom'>". ucfirst($nom).": <span class='error'></span></label>";
            if($type === 'textarea'){
                echo "<textarea id='$nom' name='$nom'></textarea>";
            }elseif ($type === 'date'){
                echo "<input type='$type' name='$nom-date' id='$nom-date' value='". date('Y-m-d')."'/>";
                echo "<input type='time' name='$nom-time' id='$nom-time' value='". date('G:i:s')."'/>";
            }else{
                echo "<input type='$type' name='$nom' id='$nom'/>";
            }
        }
        foreach ($tableHidden as $name => $value){
            echo "<input type='hidden' name='$name' value='$value'/>";
        }
        echo "<input type='submit' value='Valider'/>";
        echo '</form>';
        $this->html .= ob_get_contents();
        ob_end_clean();
    }
    
    public function listeObjet($tabObjet,$template){
        // Role: créer une liste à partir d'un tableau d'objet
        // Param: Néant
        // Retour: Néant
        ob_start();
        foreach ($tabObjet as $objet){
            include 'template/fragment/'.$template.'.php';
        }
        $this->html .= ob_get_contents();
        ob_end_clean();
    }
    
    public function select($label,$id,$tabObjet,$champOption){
        // Role: Créer un select depuis un tableau
        // Param: $nom->le nom 
        //        $tabObjet-> tableau d'objet
        //        $champOption-> Champ utilisé pour l'option
        //        $id-> id du select
        // Retour: Néant
        ob_start();  
        echo "<label for='$id'>". ucfirst($label).": </label>";
        echo "<select id='$id'>";
        foreach ($tabObjet as $objet){
            echo"<option value='".$objet->get('id')."'>". ucfirst($objet->get($champOption,true))."</option>";
        }
        echo "</select>";
        $this->html .= ob_get_contents();
        ob_end_clean();  
    }
    
    public function hiddenInput($id,$value){
        // Role: créer un input de type hidden
        // Param: $id-> id de l'input
        // Retour: Néant
        ob_start();
        echo "<input type='hidden' id='$id' value='$value'/>";
        $this->html .= ob_get_contents();
        ob_end_clean();
    }
}
