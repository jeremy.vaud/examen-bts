<?php

/*
 * Classe static de la base de donnée
 */

class BDD {
    
    // Attributs
    protected static $DB = 'mysql:host=localhost;dbname=projets_tickets_jvaud;charset=UTF8';
    protected static $USER = 'jvaud-exam';
    protected static $PASSWORD = '**********';
    protected static $OPTION = [PDO::ATTR_ERRMODE , PDO::ERRMODE_WARNING];
    protected static $PDO;
    protected static $REQ;
    
    // Méthodes
    public static function Connexion(){
        // Role: Connexion à la base de donnée
        // Param: Néant
        // Retour: Néant
        try {
            self::$PDO = new PDO(self::$DB, self::$USER, self::$PASSWORD, self::$OPTION);
        } catch (PDOException $exc) {
            print 'Error : '.$exc->getMessage();
            die();
        }
    }
    
    public static function Execute($sql,$param){
        // Role: Executer une requete sql
        // Param: $sql -> la requete sql
        //        $param -> les parametres de la requete
        // Retour: True si la requete est executé, false sinon
        if(!self::$PDO){
            self::Connexion();
        }
        self::$REQ = self::$PDO->prepare($sql);
        if(self::$REQ->execute($param)){
            return true;
        }
        return false;
    }
    
    public static function Fetch(){
        // Role: Recuperer une ligne de résultat d'une requete
        // Param: Néant
        // Retour: La ligne de résultat
        return self::$REQ->fetch(PDO::FETCH_ASSOC);
    }
    
    public static function FetchAll(){
        // Role: Recuperer le tableau des résultats d'une requete
        // Param: Néant
        // Retour: Le tableau des résultats
        return self::$REQ->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public static function LastInsertId(){
        // Role: Recuperer l' id de la derniere insertion
        // Param: Néant
        // Retour: id de la derniere insertion
        return self::$PDO->lastInsertId();
    }
    
    public static function RowCount(){
        // Role: Compter le nombre de ligne affecté par la requete
        // Param: Néant
        // Retour: nombre de ligne
        return self::$REQ->rowCount();
    }
}
