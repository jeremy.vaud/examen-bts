<?php

/*
 * Classe utilisateur
 */

class utilisateur extends table{
    
    // Constructeur
    public function __construct($id = 0){
        // Role: Construction d'un objet utilisateur
        // Param: Néant
        // Retour: Néant
        $this->champ = ["nom"=>"","prenom"=>"","email"=>"","password"=>"","statu"=>"","recovery"=>""];
        $this->link = [];
        parent::__construct($id);
    }
    
    // Méthodes
    public function connect($password){
        // Role: Vérifier l'email et le mot de passe de l'utilisateur
        // Param: $password->le mot de passe
        // Retour: "error-execute"-> requete echoué
        //         "error-email" -> pas de compte trouver
        //         "error-pass"-> mauvais mot de pass
        //         "valid"-> vérification validé
        $sql = "SELECT * FROM `utilisateur` WHERE `email` = :email";
        $param = [":email" => $this->champ['email']];
        if(BDD::Execute($sql, $param)){
            $ligne = BDD::Fetch();
            if(!$ligne){
                return "error-email";
            }
            if(!password_verify($password, $ligne['password'])){
                return "error-password";
            }
            $this->setFromTab($ligne);
            return "valid";          
        }else{
            return "error-execute";
        }
    }
    
    
    
    
    public function search($texte){
        // Role: Rechercher des utilisateurs client en fonction d'une chaine de caractere (sur email,nom,ou prenom)
        // Param: $texte-> le texte recherché
        // Retour: $result-> tableau de résultat
        $sql = "SELECT * FROM `utilisateur` WHERE (`email` = :email OR `nom` LIKE :nom OR `prenom` Like :prenom) AND `statu` = 'client'";
        $param = [":email" => $texte,":nom" => "%$texte%",":prenom"=>"%$texte%"];
        $result = [];
        if(BDD::Execute($sql, $param)){
            $table = BDD::FetchAll();
            foreach ($table as $tabUtilisateur){
                $utilisateur = new utilisateur;
                $utilisateur->setFromTab($tabUtilisateur);
                $result[] = $utilisateur;
            }
        }
        return $result;
    }
    
    public function availableEmail(){
        // Role: Vérifier qu' un email n'est pas deja utilisé
        // Param: Néant
        // Retour: true si utilisé false sinon
        $sql = "SELECT * FROM `utilisateur` WHERE `email` = :email";
        $param = [":email" => $this->champ['email']];
        if(BDD::Execute($sql, $param)){
            $ligne = BDD::Fetch();
            if(!$ligne){
                return true;
            }
        }
        return false;
    }
}
