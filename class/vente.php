<?php

/*
 * Classe vente
 */

class vente extends table{
    
    // Constructeur
    public function __construct($id = 0){
        // Role: Construction d'un objet vente
        // Param: Néant
        // Retour: Néant
        $this->champ = ["utilisateur"=>"","produits"=>"","numserie"=>"","date"=>""];
        $this->link = ["utilisateur"=>new utilisateur,"produits"=>new produits];
        parent::__construct($id);
    }
    
    public function loadFromUtilisateur($utilisateur){
        // Role: Charger les ventes objet depuis son utilisateur
        // Param: $utilisateur -> id de l'utilisateur
        // Retour: $result->tableu des ventes
        $sql = "SELECT".$this->joinSql()." WHERE `vente`.`utilisateur` = :utilisateur";
        $param = [":utilisateur" => $utilisateur];
        $result = [];
        if(BDD::Execute($sql, $param)){
            $table = BDD::FetchAll();
            foreach ($table as $tabVente){
                $vente = new vente;
                $vente->setSql($tabVente);
                $result[] = $vente;
            }
        }
        return $result;
    }
}
