<?php

/*
 * Classe message
 */

class message extends table{
    
    // Constructeur
    public function __construct($id = 0){
        // Role: Construction d'un objet message
        // Param: Néant
        // Retour: Néant
        $this->champ = ["utilisateur"=>"","date"=>"","ticket"=>"","message"=>""];
        $this->link = ["utilisateur"=>new utilisateur,"ticket"=>new ticket];
        parent::__construct($id);
    }
    
    public function loadMessagerie($ticket){
        // Role: Charger les message lier à un ticket
        // Param: $ticket-> id ticket
        // Retour: $result->tableu des message trier par date
        $sql = "SELECT".$this->joinSql()." WHERE `message`.`ticket` = :ticket ORDER BY `message`.`date`";
        $param = [":ticket" => $ticket];
        $result = [];
        if(BDD::Execute($sql, $param)){
            $table = BDD::FetchAll();
            foreach ($table as $tabMsg){
                $msg = new message;
                $msg->setSql($tabMsg);
                $result[] = $msg;
            }
        }
        return $result;    
    }
}
