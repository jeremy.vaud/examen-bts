<?php

/*
 * Class mère
 */

class table {
    
    //Attributs
    protected $id; // Clée cprimaire
    protected $champ; // Tableau des champs de l'objet
    protected $link; // Tableau des objets lié à l'objet
    
    // Constructeur
    public function __construct($id){
        // Role: Construction d'un objet
        // Param: Néant
        // Retour: Néant
        if($id > 0){
            $this->loadFromId($id);
        }       
    }
    
    // Getters
    public function get($name,$html = false){
        // Role: Obtenir la valeur d'un champ
        // Param: $name->nom du champ
        //        $html-> si true les caractère sont converti en entites HTML
        // Retour: $value-> valeur du champ
        $value = "";
        if($name === 'id'){
            $value = $this->id;
        }elseif (isset ($this->champ[$name])){
            $value = $this->champ[$name];
        }
        if($html){
            if($name === 'date'){
                $explode = explode(' ',$value);
                $explodeDate = explode('-',$explode[0]);
                return htmlentities('Le '.$explodeDate[2].'/'.$explodeDate[1].'/'.$explodeDate[0].' à '.$explode[1]);
            }
            return htmlentities($value);
        } else {
            return $value;
        }
    }

    public function getLink($name){
        // Role: Retourner un objet du tableau link
        // Param: $name-> nom de l'objet
        // Retour: $objet-> l'objet
        $objet = "";
        if(isset($this->link[$name])){
            $objet = $this->link[$name];
        }
        return $objet;
    }

    public function getNomChamp(){
        // Role: Obtenir la liste des noms des champs de l'objet
        // Param: Néant
        // Retour: $table-> tableau des noms des champs
        $table = ['id'];
        foreach (array_keys($this->champ) as $champ){
            $table[] = $champ;
        }
        return $table;
    }

    // Setters
    public function set($name,$value){
        // Role: Affecter une valeur à un champ
        // Param: $name->nom du champ
        //        $value-> valeur du champ
        // Retour: Néant
        if($name === 'id'){
            $this->id = $value;
        }elseif(isset($this->champ[$name])){
            $this->champ[$name] = $value;
        }
    }
    
    public function setFromTab($table){
        // Role: Affecter des valeurs aux champs de puis un tableau
        // Param: $table -> le tableau
        // Retour: Néant
        foreach ($table as $name => $value) {
            $this->set($name, $value);
        }
    }


    // Méthode
    public function loadFromId($id) {
        // Role: Charger un objet depuis son id
        // Param: $id -> id de l'objet
        // Retour: True si tous s'est bien passé, false sinon
        $class = get_class($this);
        $sql = "SELECT".$this->joinSql()." WHERE `$class`.`id` = :id";
        $param = [":id" => $id];
        if(BDD::Execute($sql, $param)){
            $ligne = BDD::Fetch();
            
            if(!$ligne){
                return false;
            }
            $this->setSql($ligne);
            return true;
        }
        return false;
    }
    
    public function liste() {
        // Role: Lister tous les objet de la bdd
        // Param: Néant
        // Retour: $result-> tableau d'objet
        $class = get_class($this);
        $sql = "SELECT".$this->joinSql();
        $param = [];
        $result = [];
        if(BDD::Execute($sql, $param)){
            $table = BDD::FetchAll();
            foreach ($table as $ligne){
                $objet = new $class; 
                $objet->setSql($ligne);
                $result[] = $objet;
            }
            
        }
        return $result;
    }
    
    public function insert(){
        // Role: Inserer une nouvelle ligne dans la base de donnée
        // Param: Néant
        // Retour: True si tous s'est bien passé, false sinon
        $class = get_class($this);
        $sql = "INSERT INTO `$class` SET ";
        $param = [];
        foreach ($this->champ as $champ => $value) {
            $sql .= "`$champ` = :$champ,";
            $param[":$champ"] = $value;
        }
        $sql = substr($sql, 0, -1);
        if(BDD::Execute($sql, $param)){
            $this->id = BDD::LastInsertId();
            return true;
        }
        return false;
    }
    
    public function update(){
        // Role: Mettre à jour une ligne de la bdd
        // Param: Néant
        // Retour: True si tous s'est bien passé, false sinon
        $class = get_class($this);
        $sql = "UPDATE `$class` SET ";
        $param = [":id" => $this->id];
        foreach ($this->champ as $champ => $value) {
            $sql .= "`$champ` = :$champ,";
            $param[":$champ"] = $value;
        }
        $sql = substr($sql, 0, -1)." WHERE `id`=:id";       
        if(BDD::Execute($sql, $param)){
            if(BDD::RowCount() === '0'){
                return false;
            }
            return true;
        }
        return false;
    }
    
    // Méthodes pour les jointures sql
    public function joinSql(){
        // Role: créer la partie jointure d'une requete sql
        // Param: Néant
        // Retour: $sql-> la partie jointure de la requete
        $class = get_class($this);
        $sql = " `$class`.`id` AS `$class"."_id`,";
        $join = "";
        foreach (array_keys($this->champ) as $champ){
            $sql .= " `$class`.`$champ` AS `$class"."_"."$champ`,";
        }
        foreach ($this->link as $link => $objet){
            $tabChamp = $objet->getNomChamp(); 
            $join .= " LEFT JOIN `$link` ON `$class`.`$link` = `$link`.`id`";
            foreach ($tabChamp as $champ){
                $sql .=  " `$link`.`$champ` AS `$link"."_"."$champ`,";
            }
        }
        $sql = substr($sql, 0,-1)." FROM `$class`".$join;
        return $sql;
    }
    
    public function setSql($ligne){
        // Role: Affecter les resultat d'une requete sql avec jointure à l'objet
        // Param: $ligne -> ligne de resultat
        // Retour: Néant
        $class = get_class($this);
        foreach ($ligne as $champ => $value){
            $explode = explode('_', $champ);
            if($explode[0] === $class){
                $this->set($explode[1], $value);
            } else{
                $this->getLink($explode[0])->set($explode[1], $value);
            }
        }
    }
}
