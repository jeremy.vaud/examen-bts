<?php

/*
 * Classe ticket
 */

class ticket extends table{
    
    // Constructeur
    public function __construct($id = 0){
        // Role: Construction d'un objet ticket
        // Param: Néant
        // Retour: Néant
        $this->champ = ["vente"=>"","statu"=>"","date"=>"","demande"=>"","newmsg"=>"","utilisateur"=>"",'titre'=>''];
        $this->link = ["vente"=>new vente,"utilisateur"=> new utilisateur];
        parent::__construct($id);
    }
    
    public function loadFromVente($id) {
        // Role: Charger un ticket depuis l'id de sa vente
        // Param: $id -> id de la vente
        // Retour: True si tous s'est bien passé, false sinon
        $sql = "SELECT".$this->joinSql()." WHERE `ticket`.`vente` = :id";
        $param = [":id" => $id];
        if(BDD::Execute($sql, $param)){
            $ligne = BDD::Fetch();
            if(!$ligne){
                return false;
            }
            $this->setSql($ligne);
            return true;
        }
        return false;
    }
    
    
    public function loadNotCloseFromUtilisateur($utilisateur){
        // Role: Lister tous les ticket non fermé et correspondant à un utilisateur et les trier (reponse/attente reponse)
        // Param: Néant
        // Retour: $result-> tableau des ticket
        $class = get_class($this);
        $sql = "SELECT".$this->joinSql()."WHERE `ticket`.`utilisateur` = :utilisateur && `ticket`.`statu` != 'fermer' ORDER BY `ticket`.`date` DESC";
        $param = [":utilisateur"=>$utilisateur];
        $result = ['reponse'=>[],'attente'=>[]];
        if(BDD::Execute($sql, $param)){
            $table = BDD::FetchAll();
            foreach ($table as $ligne){
                $ticket = new ticket;
                $ticket->setSql($ligne);
                switch ($ticket->get('newmsg')){
                    case 'client':
                        $result['attente'][]= $ticket;
                        break;
                    default:
                        $result['reponse'][]= $ticket;
                        break;
                }
                
                
            }
            
        }
        return $result;
    }
    
    public function ticketForTechnician(){
        // Role: Récuperer tous les ticket non fermé et les trier en 3 categorie
        // Param: Néant
        // Retour: $result-> tableau des ticket trié
        $sql = "SELECT".$this->joinSql()."WHERE `ticket`.`statu` != 'fermer' ORDER BY `ticket`.`date` DESC";
        $param = [];
        $result = ['ouvert'=>[],'newmsg'=>[],'attente'=>[]];
        if(BDD::Execute($sql, $param)){
            $table = BDD::FetchAll();
            foreach ($table as $ligne){
                $ticket = new ticket;
                $ticket->setSql($ligne);
                switch ($ticket->get('statu')){                    
                    case 'ouvert':
                        $result['ouvert'][] = $ticket;
                        break;                    
                    case 'en cours':
                        switch ($ticket->get('newmsg')){                       
                            case 'client':
                                $result['newmsg'][] = $ticket;
                                break;
                            case 'technicien':
                                $result['attente'][] = $ticket;
                                break;
                        }
                        break;
                }
            }   
        }
        return $result;    
    }
}
