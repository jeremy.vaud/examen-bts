<?php
/*
 * Insertion nouveau message et envoi liste des message 
 */
include 'lib/init.php';
try {
    if(!isConnect()){
        throw new Exception('error');
    }
    $utilisateur = new utilisateur($_SESSION['id']);
    if(!($utilisateur->get('statu') === 'client' || $utilisateur->get('statu') === 'technicien')){
        throw new Exception('error');
    }
    $message = new message;
    $message->setFromTab($_POST);
    $message->set('date', date('Y-m-d H:i:s'));
    if(!$message->insert()){
        throw new Exception('error');
    }
    $ticket = new ticket($_POST['ticket']);
    if($utilisateur->get('statu') === 'technicien' && $ticket->get('statu') === 'ouvert'){
        $ticket->set('statu', 'en cours');
        $ticket->set('newmsg', 'technicien');
        $ticket->update(); 
    }elseif($utilisateur->get('statu') !== $ticket->get('statu')){
        $ticket->set('newmsg', $utilisateur->get('statu'));
        $ticket->update();
    }
    $tabMsg = $message->loadMessagerie($_POST['ticket']);
    $data = new template;
    $data->listeObjet($tabMsg, 'list-message');
    $data->display(); 
} catch (Exception $exc) {
    echo $exc->getMessage();
}
