<?php
/*
 * Index
 */
include 'lib/init.php';
if(!isConnect()){
    // Non connecté on affiche l'écran de connexion
    $message = "Vous devez vous connecter pour accéder à cette page";
    $header = new template;
    $header->setTemplate('header/notConnect');  
    $form = new template;
    $form->form(["email"=>"texte","password"=>"password"], 'connexion.php');
    $titre = 'Connexion à votre compte';
    include 'template/page/form.php';
}else{
    if(isset($_GET['msg'])){
        // On vient de se connecter
        $message = "Vous êtes maintenant connecté";
    }
    $utilisateur = new utilisateur($_SESSION['id']);
    try {
        $header = new template;
        $header->setTemplate('header/connect');
        $main = new template;
        // Mode d'interface 
        if($utilisateur->get('statu') === 'vendeur'){ 
            // interface vendeur
            $main->setTemplate('acc-vendeur');
            $script = "js/vendeur.js";
            include 'template/page/accueil.php';        
        } elseif ($utilisateur->get('statu') === 'client') {
            // interface client
            $ticket = new ticket;
            $tabTicket = $ticket->loadNotCloseFromUtilisateur($_SESSION['id']);
            $newAnswer = new template();
            $waitAnswer = new template();
            $newAnswer->listeObjet($tabTicket['reponse'], 'list-ticket');
            if($newAnswer->get() === ""){
                $newAnswer->setTemplate('noAnswer');
            }
            $waitAnswer->listeObjet($tabTicket['attente'], 'list-ticket');
            if($waitAnswer->get() === ""){
                $waitAnswer->setTemplate('noAnswer');
            }
            $vente = new vente;
            $tabVente = $vente->loadFromUtilisateur($_SESSION['id']);          
            $achat = new template;
            $achat->listeObjet($tabVente, 'list-achat');           
            include 'template/page/acc-client.php';       
        } elseif ($utilisateur->get('statu') === 'technicien') {
            // interface technicien
            $ticket = new ticket;
            $tabTicket = $ticket->ticketForTechnician();
            $newTicket = new template;
            $newMsg = new template;
            $attente = new template;
            $newTicket->listeObjet($tabTicket['ouvert'], 'list-ticket');
            $newMsg->listeObjet($tabTicket['newmsg'], 'list-ticket');          
            $attente->listeObjet($tabTicket['attente'], 'list-ticket');
            if($newTicket->get() === ""){
                $newTicket->setTemplate('noTicket');
            }
            if($newMsg->get() === ""){
                $newMsg->setTemplate('noTicket');                
            }
            if($attente->get() === ""){
                $attente->setTemplate('noTicket');
            }
            include 'template/page/acc-technician.php';   
        }else{
            deconnexion();
            throw new Exception("Une erreur est survenu, veuillez vous connecter");
        }
    } catch (Exception $exc) {
        $message = $exc->getMessage();
        $header->setTemplate('header/notConnect');  
        $form = new template;
        $form->form(["email"=>"texte","password"=>"password"], 'connexion.php');
        $titre = 'Connexion à votre compte';
        include 'template/page/form.php';        
    }

    
}        